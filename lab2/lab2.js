﻿'use strict';
//1
////Разработайте функцию getType, которая принимает единственный параметр
////и возвращает тип этого параметра

function getType(x) {
  if (x === null) return 'null';
  return typeof(x);
}

//2
////Разработайте функцию askForAge которая задает пользователю вопрос "Введите ваш возраст"
////и в зависимости от возраста возвращает возрастную категорию
////до 18 , от 18 до 25, от 25 до 50, от 50 и старше
////в случае если пользователь не указал свой возраст функция возвращает "Возраст не определен"

function askForAge() {
  var age = parseInt(prompt('Введите ваш возраст'));
  if (age < 18) return 'до 18';
  if (age >= 18 && age < 25) return 'от 18 до 25';
  if (age >= 25 && age < 50) return 'от 25 до 50';
  if (age > 50) return 'от 50 и старше';
  return 'Возраст не определен';
}
//3
////Разработайте функцию doubleFactorial для расчета двойного факториала числа
////В случае невозможности расчета функиция возвращает null
////Двойной факториал n!!=n*(n-2)*(n-4)*...*2 (или 1)
////функция принимает один параметр

function doubleFactorial(n) {
  if (typeof(n) !== 'number') return null;
  if (n < 1) return null;

  const range = function(from, to, step) {
    var range = [];
    while (from > to) {
      range.push(from);
      from -= step;
    }
    return range;
  };

  const multiply = function(a,b) { return a * b; };

  return range(n, 1, 2).reduce(multiply, 1);
}
//4
////Разработайте функцию getCharCount которая принимает символ и строку
////и возвращает количество таких символов в строке
////если вместо символа передана строка, то в качестве символа используется первый символ этой строки.
////регистр значения не имеет

function getCharCount(chars, str) {
  const matchingChar = function(ch) {
    return function(x) {
      return ch.toLowerCase() === x.toLowerCase();
    };
  };

  var firstCh = chars[0] || chars;
  return str.split('').filter(matchingChar(firstCh)).length;
}
//5
////разработайте функцию askForNumber, которая просит пользователя ввести число больше 10
////если пользователь ввел меньшее число, то функция просит повторно ввести число.
////если число больше 10, то выводится сообщение (alert) "Вы ввели {число}}"
//// Например: "Вы ввели 11"

function askForNumber() {
  var n = parseInt(prompt('Введите число больше 10'));

  while (isNaN(n) || n <= 10) {
    n = parseInt(prompt('Введите число больше 10'));
  }

  alert('Вы ввели ' + n);
};

//6
////разработайте функцию isNumeric которая принимает один аргумент n.
////если n действительное число, то возвращает true
////иначе функция возвращает false

function isNumeric(n) {
  return !((n === null) || (typeof(n) === 'boolean')) && !isNaN(Number(n));
}

//7
////Разработайте функцию toHex которая принимает аргумент
////если аргумент действительное число, то возвращает строку соответстующую этому числу в 16-риченой системе
////в остальных случаях возвращает строковое предствление аргумента

function toHex(n){
  if (isNumeric(n)) {
    return parseInt(n).toString(16);
  } else {
    return String(n);
  }
}
//8
////Разработайте функцию getDecimal которая принимает 1 параметр - число плавающей точкой (например 1.25)
////и возвращает его дробную часть getDecimal(1.25)=0.25

function getDecimal(n){
  const str = n.toString();
  return Number('0' + str.slice(str.indexOf('.'), str.length));
}

//9
////fibonachi которая принимает число и возвращает ряд фибоначчи до этого числа в виде строки
////например для числа 10  результат "1,1,2,3,5,8"

function fibonachi(n) {
  var sequence = [],
      next = 1;

  while(next < n) {
    sequence.unshift(next);
    next = sequence[0] + (sequence[1] || 0);
  }

  return sequence.reverse().join(',');
}

//10
////Разработайте функцию changeCase, которая принимает 1 аргумени
////если аргумент не строка, то преобразует его в строку.
////в полученной строке заменяем регистр у каждого из символов
////Например changeCase("Строка") -- "сТРОКА"

function changeCase(str){
  function toggleCharCase(ch) {
    if (ch === ch.toLowerCase()){
      return ch.toUpperCase();
    } else {
      return ch.toLowerCase();
    }
  }

  return String(str).split('').map(toggleCharCase).join('');
}

//11
////Разработайте функцию round, которая позволяет округлять число
////до указанного количества знаков знаков после запятой
////функция принимает 2 параметра
////перевый параметр -- число
////второй количество знаков после запятой (может быть отрицательным)

// le php round :)
function round(n, precision) {
  function e(pr) {
    return Math.pow(10, pr);
  }

  return Math.round(n * e(precision)) / e(precision);
}


//12
////Разработайте функцию contains, которая принимает 2 строки в качестве аргументов
////функция возвращает true, если втророй аргумент является подстрокой первого аргумента
////null если один или оба аргумента -- пустые строки
////иначе false

function contains(a, b) {
  if (a.length === 0 || b.length === 0) return null;
  return a.indexOf(b) >= 0;
}

//13
////Разработайте функцию substringCount, которая принимает 2 строки в качестве аргументов
////функция возвращает количество вхождений второй строки в первую
////если одна из строк пустая то 0

function substringCount(a, b) {
  if (!contains(a,b)) return 0;
  return a.split(b).length - 1;
}


//14
////Разработайте функцию compare, которая производит сравнение строк с учетом или без учета регистра
////параметры 1 и 2 - это сравниваемые строки. Параметр 3 логический флаг
////true -- регистр учитывается, false -- нет

function compare(a, b, caseSensitive) {
  var c = a, d = b;

  if (!caseSensitive) {
    c = a.toLowerCase();
    d = b.toLowerCase();
  }
  return c === d;
}


//15
////Создайте функцию truncate(str, maxlength), которая проверяет длину строки str,
////и если она превосходит maxlength – заменяет конец str на "...", так чтобы ее длина стала равна maxlength.
////Результатом функции должна быть (при необходимости) усечённая строка.
////например
////truncate("Вот, что мне хотелось бы сказать на эту тему:", 20) = "Вот, что мне хоте..."
////truncate("Всем привет!", 20) = "Всем привет!"

function truncate(str, maxlength) {
  var max = parseInt(maxlength);
  if (str.length < max) return str;
  return str.slice(0, max - 3) + '...';
}
